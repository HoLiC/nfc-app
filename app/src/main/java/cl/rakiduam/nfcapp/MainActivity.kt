package cl.rakiduam.nfcapp

import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {

    private var mNfcAdapter: NfcAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this)

        if(intent != null && intent.action != Intent.ACTION_MAIN) onNewIntent(intent)
    }

    override fun onResume() {
        super.onResume()
        mNfcAdapter?.let {
            NFCUtil.enableNFCInForeground(it, this, javaClass)
        }
    }

    override fun onPause() {
        super.onPause()
        mNfcAdapter?.let {
            NFCUtil.disableNFCInForeground(it, this)
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        AlertDialog.Builder(this)
                .setMessage("Do you want to write to NFC?")
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("Write to NFC") { _, _ ->
                    val messageWrittenSuccessfully = NFCUtil.createNFCMessage(
                            messageEditText.text.toString(), intent)
                    toast(messageWrittenSuccessfully.ifElse(
                            "Successful Written to Tag",
                            "Something When wrong Try Again"))
                }
                // negative button text and action
                .setNegativeButton("Show Content") { dialog, _ ->
                    val message = NFCUtil.retrieveNFCMessage(intent)
                    tvReadNFC.text = message
                    Log.d("SH/NFC", message)
                    dialog.cancel()
                }
                .create()
                .show()
    }
}

fun <T> Boolean.ifElse(primaryResult: T, secondaryResult: T) = if (this) primaryResult else secondaryResult