package cl.rakiduam.nfcapp

import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import android.util.Log
import java.io.IOException

/**
 * Created by PJ Welcome on 2017/07/08 in NFCApp.
 */
object NFCUtil {

    fun createNFCMessage(payload: String, intent: Intent?): Boolean {
        val pathPrefix = "rakiduam.cl:nfc"
        val nfcRecord = NdefRecord(NdefRecord.TNF_EXTERNAL_TYPE, pathPrefix.toByteArray(), ByteArray(0), payload.toByteArray())
        val nfcMessage = NdefMessage(arrayOf(nfcRecord))
        intent?.let {
            val tag = it.getParcelableExtra<Tag>(NfcAdapter.EXTRA_TAG)
            return writeMessageToTag(nfcMessage, tag)
        }
        return false
    }

    fun retrieveNFCMessage(intent: Intent?): String {
        intent?.let {
            Log.d("SH/NFC", "INTENT ACTION " + intent.action)
            when (intent.action) {
                NfcAdapter.ACTION_NDEF_DISCOVERED -> {
                    val nDefMessages = getNDefMessages(intent)
                    nDefMessages[0].records?.let {
                        it.forEach {
                            Log.d("SH/NFC", "NEW RECORD")
                            Log.d("SH/NFC", "NEW PAYLOAD " + it?.toUri()?.toString())
                            it?.payload.let {
                                it?.let {
                                    Log.d("SH/NFC", String(it))
                                    return String(it)
                                }
                            }
                        }
                    }
                }
                NfcAdapter.ACTION_TECH_DISCOVERED -> {
                    val nDefMessages = getNDefMessages(intent)
                    nDefMessages?.forEach {
                        it?.records.let {
                            it.forEach {
                                Log.d("SH/NFC", "NEW RECORD")
                                Log.d("SH/NFC", "NEW PAYLOAD: " + it?.toUri()?.toString())
                                it?.payload.let {
                                    it?.let {
                                        Log.d("SH/NFC", String(it))
                                        return String(it)
                                    }
                                }
                            }
                        }
                    }
                }
                else -> {
                    return "ninguno"
                }
            }
        }
        return "ninguno"
    }


    private fun getNDefMessages(intent: Intent): Array<NdefMessage> {
        val rawMessage = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
        rawMessage?.let {
            return rawMessage.map {
                it as NdefMessage
            }.toTypedArray()
        }
        // Unknown tag type
        val empty = byteArrayOf()
        val record = NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, empty)
        val msg = NdefMessage(arrayOf(record))
        return arrayOf(msg)
    }

    fun disableNFCInForeground(nfcAdapter: NfcAdapter, activity: Activity) {
        nfcAdapter.disableForegroundDispatch(activity)
    }

    fun <T> enableNFCInForeground(nfcAdapter: NfcAdapter, activity: Activity, classType: Class<T>) {
        val pendingIntent = PendingIntent.getActivity(activity, 0,
                Intent(activity, classType).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0)
        val filters = arrayOf(
                IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED),
                IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED),
                IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED)
        )

        val techLists = arrayOf(arrayOf(Ndef::class.java.name), arrayOf(NdefFormatable::class.java.name))

        nfcAdapter.enableForegroundDispatch(activity, pendingIntent, null, null)
    }


    private fun writeMessageToTag(nfcMessage: NdefMessage, tag: Tag?): Boolean {

        try {
            val nDefTag = Ndef.get(tag)

            nDefTag?.let {
                it.connect()
                if (it.maxSize < nfcMessage.toByteArray().size) {
                    //Message to large to write to NFC tag
                    return false
                }
                return if (it.isWritable) {
                    it.writeNdefMessage(nfcMessage)
                    it.close()
                    //Message is written to tag
                    true
                } else {
                    //NFC tag is read-only
                    false
                }
            }

            val nDefFormatableTag = NdefFormatable.get(tag)

            nDefFormatableTag?.let {
                return try {
                    it.connect()
                    it.format(nfcMessage)
                    it.close()
                    //The data is written to the tag
                    true
                } catch (e: IOException) {
                    //Failed to format tag
                    false
                }
            }
            //NDEF is not supported
            return false

        } catch (e: Exception) {
            //Write operation has failed
        }
        return false
    }
}